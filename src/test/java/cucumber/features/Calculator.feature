Feature: LKF calculator

  Scenario: button is disabled when security checkbox is unchecked
    Given user on LKF calculator website
    And all default input fields present on page
    When permission checkbox is unchecked
    Then user can not press button
    And close LKF calculator website

  Scenario: button is enabled when security checkbox is checked
    Given user on LKF calculator website
    And all default input fields present on page
    When permission checkbox is checked
    Then user can press button
    And close LKF calculator website

  Scenario: all insurance companies show price
    Given user on LKF calculator website
    And all default input fields present on page
    And correct data is filled into fields
    When permission checkbox is checked
    And button is pressed
    Then all insurance companies prices are shown
    And close LKF calculator website
