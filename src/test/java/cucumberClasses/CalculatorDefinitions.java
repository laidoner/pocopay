package cucumberClasses;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import framework.Calculator;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;



public class CalculatorDefinitions {
	Calculator calc = new Calculator();	
	
	@Given("^user on LKF calculator website$")
	public void user_on_LKF_calculator_website() throws Throwable {
	    calc.navigate();
	}

	@Given("^all default input fields present on page$")
	public void all_default_input_fields_present_on_page() throws Throwable {		
	    assertTrue(calc.areDefaultFieldsPresent());	    
	}
	
	@Given("^correct data is filled into fields$")
	public void correct_data_is_filled_into_fields() throws Throwable {
	    calc.fillData();
	}

	@When("^permission checkbox is unchecked$")
	public void permission_checkbox_is_unchecked() throws Throwable {
	    assertFalse(calc.permissionCheckBoxStatus());
		
	}
	
	@When("^permission checkbox is checked$")
	public void permission_checkbox_is_checked() throws Throwable {
	    calc.activatePermissionCheckbox();	    
	}
	
	@When("^button is pressed$")
	public void button_is_pressed() throws Throwable {
	    calc.pressButton();
	}

	@Then("^user can not press button$")
	public void user_can_not_press_button() throws Throwable {
		assertFalse(calc.isButtonActive());
	}	

	@Then("^user can press button$")
	public void user_can_press_button() throws Throwable {
		assertTrue(calc.isButtonActive());	    
	}

	@Then("^close LKF calculator website$")
	public void close_LKF_calculator_website() throws Throwable {
	    calc.closeBrowserWindow();
	}
	
	

	

	@Then("^all insurance companies prices are shown$")
	public void all_insurance_companies_prices_are_shown() throws Throwable {
		assertTrue(calc.showAllCompaniesPrices());
	}
}
