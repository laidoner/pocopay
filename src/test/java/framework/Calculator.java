package framework;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Calculator {
	public Calculator() {
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}

	public void navigate() {
		driver.get("https://hermes.lkf.ee/");
	}

	public boolean areDefaultFieldsPresent() {
		boolean isExists = true;
				
		try {
			driver.findElement(By.id("ownerCode"));
			driver.findElement(By.id("registrationCode"));
			//....
		} catch (NoSuchElementException e) {
			isExists = false;			
		}

		return isExists;
	}

	public boolean permissionCheckBoxStatus()
	{		
		return driver.findElement(By.id("permission")).isSelected();
	}
	
	public boolean isButtonActive()
	{
		return driver.findElement(By.id("search_submit")).isEnabled();
	}
	
	public void activatePermissionCheckbox()
	{
		driver.findElement(By.id("permission")).click();
	}
	
	public void fillData()
	{
		driver.findElement(By.id("ownerCode")).sendKeys("38412270323");;
		driver.findElement(By.id("registrationCode")).sendKeys("899MLG");
		Select select = new Select(driver.findElement(By.id("location")));
		select.selectByValue("TALLINN");				
	}
	
	public void pressButton()
	{
		driver.findElement(By.id("search_submit")).click();		
	}
	
	public void closeBrowserWindow()
	{
		driver.quit();
	}
	
	public boolean showAllCompaniesPrices()
	{				
		(new WebDriverWait(driver, 20)).until(new ExpectedCondition<Boolean>() 
		{
            public Boolean apply(WebDriver d) 
            {
            	boolean condition = true;            	
            
            	condition = d.findElement(By.xpath("/html/body/div[1]/div[2]/div[3]/div/table[2]/tbody/tr[2]/td[2]")).getText().length() != 0 && 
            			    d.findElement(By.xpath("/html/body/div[1]/div[2]/div[3]/div/table[2]/tbody/tr[3]/td[2]")).getText().length() != 0 &&
            			    d.findElement(By.xpath("/html/body/div[1]/div[2]/div[3]/div/table[2]/tbody/tr[4]/td[2]")).getText().length() != 0 &&
            			    d.findElement(By.xpath("/html/body/div[1]/div[2]/div[3]/div/table[2]/tbody/tr[5]/td[2]")).getText().length() != 0 &&
            			    d.findElement(By.xpath("/html/body/div[1]/div[2]/div[3]/div/table[2]/tbody/tr[6]/td[2]")).getText().length() != 0 &&
            			    d.findElement(By.xpath("/html/body/div[1]/div[2]/div[3]/div/table[2]/tbody/tr[7]/td[2]")).getText().length() != 0 &&
                    		d.findElement(By.xpath("/html/body/div[1]/div[2]/div[3]/div/table[2]/tbody/tr[8]/td[2]")).getText().length() != 0 &&
                    		d.findElement(By.xpath("/html/body/div[1]/div[2]/div[3]/div/table[2]/tbody/tr[9]/td[2]")).getText().length() != 0;
                return condition;
            }
        });
		
		for(int i=2; i<10; i++)
		{
			String str = "/html/body/div[1]/div[2]/div[3]/div/table[2]/tbody/tr[" + Integer.toString(i) + "]/td[2]";
			String value = driver.findElement(By.xpath(str)).getText();
			value = value.replaceAll("\u20ac", "").replace(',', '.').trim();
			
			try
			{	
				if(value.equals("")) return false;
				
				Double.parseDouble(value);
			}catch(Exception e)
				{
					return false;
				}
		}
		
		return true;
	}
	
	public WebDriver driver;
}
